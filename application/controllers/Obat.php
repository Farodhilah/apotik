<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			if($this->session->userdata('login')!=TRUE){
			redirect('login','refresh');
			}
	
			$this->load->model('m_obat','obat');
		}
	public function index()
	{
		$data['tampil_obat']=$this->obat->tampil_obat();
		$data['kategori']=$this->obat->data_kategori();
		$data['judul']="Daftar obat";
		$data['konten']="obat";
		$this->load->view('template', $data);
	}

	public function tambah()
	{
		$this->form_validation->set_rules('nama_obat', 'nama_obat', 'trim|required');
		$this->form_validation->set_rules('harga', 'harga', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
		$this->form_validation->set_rules('kategori', 'kategori', 'trim|required');
		$this->form_validation->set_rules('stok', 'stok', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			$config['upload_path'] = './asset/gambar/';
			$config['allowed_types'] = 'gif|jpg|png';
		
			if($_FILES['gambar']['name']!=""){
				$this->load->library('upload', $config);
			
				if ( ! $this->upload->do_upload('gambar')){
					$this->session->set_flashdata('pesan', $this->upload->display_errors());
					redirect('obat','refresh');
				}
				else{
					if($this->obat->simpan_obat($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'sukses menambah');	
					} else {
						$this->session->set_flashdata('pesan', 'gagal menambah');	
					}
					redirect('obat','refresh');		
				}
			} else {
				if($this->obat->simpan_obat('')){
					$this->session->set_flashdata('pesan', 'sukses menambah');	
				} else {
					$this->session->set_flashdata('pesan', 'gagal menambah');	
				}
				redirect('obat','refresh');	
			}
			
		} else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('obat','refresh');
		}

	}

	public function edit_obat($id)
	{
		$data=$this->obat->detail($id);
		echo json_encode($data);
	}

	public function obat_update()
	{

		if($this->input->post('simpan')){
			if($_FILES['gambar']['name']==""){
				if($this->obat->obat_update_no_foto()){
					$this->session->set_flashdata('pesan', 'Sukses update');
					redirect('obat');
				} else {
					$this->session->set_flashdata('pesan', 'Gagal update');
					redirect('obat');
				}
			} else {
				$config['upload_path'] = './asset/gambar/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('gambar')){
					$this->session->set_flashdata('pesan', 'Gagal Upload');
					redirect('obat');
				}
				else{
					if($this->obat->obat_update_dengan_foto($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'Sukses update');
						redirect('obat');
					} else {
						$this->session->set_flashdata('pesan', 'Gagal update');
						redirect('obat');
					}
				}
			}
			
		}

	}

	public function hapus($id_obat='')
	{
		if ($this->obat->hapus_obat($id_obat)) {
			$this->session->set_flashdata('pesan', 'Sukses Hapus Obat');
			redirect('obat','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'Gagal Hapus');
			redirect('obat','refresh');
		}
	}
}

/* End of file obat.php */
/* Location: ./application/controllers/obat.php */