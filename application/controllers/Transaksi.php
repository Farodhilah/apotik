<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

public function __construct()
{
	parent::__construct();
	$this->load->model('m_transaksi','trans');
	$this->load->model('m_obat','obat');
}
	public function index()
	{
		$data['customer']=$this->trans->tm_customer();
		$data['tampil_obat']=$this->obat->tampil_obat();
		$data['judul']="Transaksi";
		$data['konten']="v_transaksi"; 
		$this->load->view('template', $data, FALSE);
	}
	public function addcart($id)
	{
		$cek_stok=$this->trans->cek($id);
		if ( $cek_stok == 0){
			$this->session->set_flashdata('pesan', 'Barang Habis');
			redirect('transaksi','refresh');
		}
			$detail=$this->obat->detail($id);

			$data = array(
				'id'      => $detail->id_obat,
				'qty'     => 1,
				'price'   => $detail->harga,
				'name'    => $detail->nama_obat,
				'options' => array('genre'=>$detail->nama_kategori)
			);
			
			$this->cart->insert($data);
			redirect('transaksi');
	}
	public function clearcart()
	{
		$this->cart->destroy();
		redirect('transaksi');
	}
	public function hapus_cart($id)
	{
		$data = array(
			'rowid' => $id,
			'qty'   => 0
		);
		
		$this->cart->update($data);
		redirect('transaksi');
	}
	public function simpan()
	{

		if ($this->input->post('update')) {
		
			for($i=0;$i<count($this->input->post('rowid'));$i++){
				$data = array(
					'rowid' => $this->input->post('rowid')[$i],
					'qty'   => $this->input->post('qty')[$i]
				);
				$this->cart->update($data);
			}
		redirect('transaksi','refresh');		
		} elseif($this->input->post('bayar')){
			$this->form_validation->set_rules('id_customer', 'customer', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id=$this->trans->simpan_cart_db();
				if($id){
					$data['nota']=$this->trans->detail_nota($id);
					$this->load->view('cetak_nota', $data, FALSE);
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('transaksi','refresh');
			}
			
		}
	}

}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */