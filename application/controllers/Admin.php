<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$data['konten']="home";
		$this->load->view('template', $data);
	}
	
	
	public function login()
	{
		if ($this->session->userdata('login')==TRUE) {
			redirect('admin','refresh');
		}else {
			$this->load->view('login');
		}
	}

	
	public function proses_login()
	{
		if ($this->input->post('login')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run()==TRUE) {
				$this->load->model('m_user');
				if ($this->m_user->get_login()->num_rows()>0) {
					$data=$this->m_user->get_login()->row();
					$array = array(
						'login' => TRUE,
						'id_pembeli' => $data->id_pembeli,
						'username' => $data->username,
						'password' => $data->password
					);
					
					$this->session->set_userdata( $array );
					redirect('admin','refresh');
				}else {
					$this->session->set_flashdata('pesan', "gagal login");	
					redirect('admin/login','refresh');
				}
			}
			else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('admin/login','refresh');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/login','refresh');
	}




	
}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */