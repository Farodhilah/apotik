<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pesanan extends CI_Model {
	public function simp_db($id,$nama_file)
	{
		$object=array(
			'bukti'=>$nama_file,
			);
		return $this->db->where('id_nota',$id)
					->update('nota', $object);
	}
	public function tm_pesanan()
	{
		return $this->db->get('nota')->result();
	}
	public function detail_pesanan($id)
	{
		return $this->db->where('id_nota',$id)->get('nota')->row();
	}
	public function trans($id_nota)
	{
		return $this->db->where('id_nota',$id_nota)
					->join('obat','obat.id_obat=transaksi.id_obat')
					->join('kategori','kategori.id_kategori=obat.id_kategori')
					->get('transaksi')->result();
	}
	public function hapus_nota($id)
	{
		$hapus_pinjam=$this->db->where('id_nota',$id)
					->delete('transaksi');
		if($hapus_pinjam){
			$hps_nota=$this->db->where('id_nota',$id)
						   ->delete('nota');
			if($hps_nota){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	

}

/* End of file M_pesanan.php */
/* Location: ./application/models/M_pesanan.php */