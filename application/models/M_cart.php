<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_cart extends CI_Model {

	public function simpan_cart()
	{
	$tgl_deadline=date('Y-m-d',mktime(date('H'),date('i'),date('s'),date('m'),date('d')+7,date('Y')));
	$object=array(
		'id_customer'=>$this->session->userdata('id_customer'),
		'tgl_beli'=>date('Y-m-d'),
		'grand_total'=>$this->input->post('grand_total'),
		'status'=>'',
		'bukti'=>''
		);
		$this->db->insert('nota', $object);
		$tm_nota=$this->db->order_by('id_nota','desc')
					  ->limit(1)
					  ->get('nota')
					  ->row();
		$hasil=array();
		for($i=0;$i<count($this->input->post('id_obat'));$i++){
			$hasil[]=array(
				'id_nota'=>$tm_nota->id_nota,
				'id_obat'=>$this->input->post('id_obat')[$i],
				'jumlah'=>$this->input->post('qty')[$i]
				);
		}
		$proses=$this->db->insert_batch('transaksi', $hasil);
		if($proses){
			return $tm_nota->id_nota;
		} else {
			return 0;
		}
	}
	public function get_total($id)
	{
		return $this->db->where('id_nota',$id)
						->get('nota')
						->row();
	}
}

/* End of file M_cart.php */
/* Location: ./application/models/M_cart.php */