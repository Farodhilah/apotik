<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaksi extends CI_Model {

	public function tm_customer()
	{
		return $this->db->get('customer')->result();
	}
	public function cek($id_obat)
	{
		$cek_stok = $this->db->where('id_obat', $id_obat)->get('obat')->row()->stok;
		if($cek_stok == 0 ){
			return 0;
		}else{
			return 1;
		}
	}

	public function check()
	{
		$cek=1;
		for($i=0;$i<count($this->input->post('rowid'));$i++){		
				$stok = $this->db->where('id_obat', $this->input->post('id_obat')[$i])
								->get('obat')
								->row()
								->stok;
				$qty = $this->input->post('qty')[$i];
				$sisa= $stok - $qty;
				if($sisa < 0){
					$oke = 0;
				}else{
					$oke = 1;
				}
				$cek = $oke * $cek;
		}
		return $cek;		
	}
	public function simpan_cart_db()
	{
		for($i=0; $i<count($this->input->post('rowid')); $i++){
				$stok = $this->db->where('id_obat', $this->input->post('id_obat')[$i])
								 ->get('obat')
								 ->row()
								 ->stok;
				$qty = $this->input->post('qty')[$i];
				$sisa = $stok - $qty;
				$updatestok = array('stok' => $sisa);
				$this->db->where('id_obat', $this->input->post('id_obat')[$i])
						 ->update('obat', $updatestok);
		}

			$object=array(
			'id_customer'=>$this->input->post('id_customer'),
			'tgl_beli'=>date('Y-m-d'),
			'grand_total'=>$this->input->post('grand_total'),
			
		);
		$this->db->insert('nota', $object);
		$tm_nota=$this->db->order_by('id_nota','desc')
					  ->where('id_customer',$this->input->post('id_customer'))
					  ->limit(1)
					  ->get('nota')
					  ->row();

		for($i=0;$i<count($this->input->post('rowid'));$i++){
				$hasil[]=array(
				'id_nota'=>$tm_nota->id_nota,
				'id_obat'=>$this->input->post('id_obat')[$i],
				'jumlah'=>$this->input->post('qty')[$i]
				);
				
			}		
			$proses=$this->db->insert_batch('transaksi', $hasil);
			if($proses){
				return $tm_nota->id_nota;
			} else {
				return 0;
			}

	}

	public function detail_nota($id_nota)
	{
		return $this->db->where('id_nota',$id_nota)
					->join('customer','customer.id_customer=nota.id_customer')
					->get('nota')->row();
	}

	public function detail_transaksi($id_nota)
	{
		return $this->db->where('id_nota',$id_nota)
					->join('obat','obat.id_obat=transaksi.id_obat')
					->join('kategori','kategori.id_kategori=obat.id_kategori')
					->get('transaksi')->result();
	}

}

/* End of file M_transaksi.php */
/* Location: ./application/models/M_transaksi.php */