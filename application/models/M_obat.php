<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_obat extends CI_Model {

	public function tampil_obat()
	{
		$tm_obat=$this->db
					  ->join('kategori','kategori.id_kategori=obat.id_kategori')
					  ->get('obat')->result();
		return $tm_obat;
	}
	
	public function data_kategori()
	{
		return $this->db->get('kategori')->result();
	}

	public function simpan_obat($nama_file)
	{
		if($nama_file==""){
			$object=array(
			'nama_obat'=>$this->input->post('nama_obat'),
			'harga'=>$this->input->post('harga'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'stok'=>$this->input->post('stok')
			);	
		} else{
			$object=array(
			'nama_obat'=>$this->input->post('nama_obat'),
			'harga'=>$this->input->post('harga'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'stok'=>$this->input->post('stok'),
			'gambar_obat'=>$nama_file
			);
		}
		return $this->db->insert('obat', $object);
	}

	public function detail($a)
	{
		$tm_obat=$this->db
					  ->join('kategori','kategori.id_kategori=obat.id_kategori')
					  ->where('id_obat',$a)
					  ->get('obat')
					  ->row();
		return $tm_obat;
	}

	public function obat_update_no_foto()
	{
		$object=array(
			'nama_obat'=>$this->input->post('nama_obat'),
			'harga'=>$this->input->post('harga'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'stok'=>$this->input->post('stok')
		);
		return $this->db->where('id_obat',$this->input->post('id_obat'))
					->update('obat', $object);
	}

	public function obat_update_dengan_foto($nama_foto='')
	{
		$object=array(
			'nama_obat'=>$this->input->post('nama_obat'),
			'harga'=>$this->input->post('harga'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'stok'=>$this->input->post('stok'),
			'gambar_obat'=>$nama_foto
		);
		return $this->db->where('id_obat',$this->input->post('id_obat'))
					->update('obat', $object);	
	}

	public function hapus_obat($id_obat='')
	{
		return $this->db->where('id_obat',$id_obat)->delete('obat');
	}

}

/* End of file M_obat.php */
/* Location: ./application/models/M_obat.php */