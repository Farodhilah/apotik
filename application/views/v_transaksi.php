<h2 align="center">Halaman Transaksi</h2><br><br>
<div class="col-md-7">
	<div class="table-agile-info">
	<table id="example" class="table table-hover table-striped">
		<thead>
			<tr>
				<th>NO</th>
				<th>Nama Obat</th>
				<th>Harga</th>
				<th>Kategori</th>
				<th>Stok</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=0; foreach ($tampil_obat as $obat): $no++; ?>
				<tr>
					<td><?=$no?></td>
					<td><?=$obat->nama_obat?></td>
					<td><?=$obat->harga?></td>
					<td><?=$obat->nama_kategori?></td>
					<td><?=$obat->stok?></td>
					<td><a class="btn btn-warning" href="<?=base_url('index.php/transaksi/addcart/'.$obat->id_obat)?>">Pesan</a></td>
				</tr>
			<?php endforeach ?>
			
		</tbody>
	</table></div>
</div>
<div class="col-md-5">
	<div class="table-agile-info">
<form action="<?=base_url('index.php/transaksi/simpan')?>" method="post">
<a class="btn btn-danger" href="<?=base_url('index.php/transaksi/clearcart')?>">Clear Cart</a>
	Nama Customer : 
	<select name="id_customer" class="form-control" style="display: inline-block;width:200px">
		<option></option>
		<?php foreach ($customer as $customer): ?>
			<option value="<?=$customer->id_customer?>"><?=$customer->nama?></option>
		<?php endforeach ?>
		
	</select>
	<table class="table table-striped table-hover">
		<tr>
			
			<th>Nama obat</th>
			<th>QTY</th>
			<th>Harga</th>
			<th>Subtotal</th>
			<th>Aksi</th>
		</tr>
		<?php $no=0; foreach ($this->cart->contents() as $items): $no++; ?>
		<input type="hidden" name="id_obat[]" value="<?=$items['id']?>">
		<input type="hidden" name="rowid[]" value="<?=$items['rowid']?>">
			<tr>
				
				<td><?=$items['name']?></td>
				<td width="1"><input type="text" name="qty[]" value="<?=$items['qty']?>" class="form-control" style="padding:4px;"></td>
				<td><?=number_format($items['price'])?></td>
				<td><?=number_format($items['subtotal'])?></td>
				<td><a href="<?=base_url('index.php/transaksi/hapus_cart/'.$items['rowid'])?>" class="btn btn-danger">&times;</a></td>
			</tr>
		<?php endforeach ?>
			<input type="hidden" name="grand_total" value="<?=$this->cart->total()?>">
			<tr style="border-bottom:5px black solid">
				<th colspan="3">Grand Total</th><th><?= number_format($this->cart->total())?></th><th></th>
			</tr>
	</table>
	<input class="btn btn-success" type="submit" name="update" value="Update QTY"> <input type="submit" onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-danger" value="Bayar" name="bayar">
</form>
<?php if ($this->session->flashdata('pesan')): ?>
	<div class="alert alert-warning"><?= $this->session->flashdata('pesan');?></div>
<?php endif ?>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
 
</script></div>