<div class="table-agile-info">
<h2 style="text-align: center;">Daftar Pesanan</h2>
<?php if ($this->session->flashdata('pesan')): ?>
	<div class="alert alert-success">
		<?= $this->session->flashdata('pesan');?>
	</div>
<?php endif ?>

<table class="table table-hover table-striped" id="example">
	<thead>
	<tr>
		<td>NO</td>
		<td>NO NOTA</td>
		<td>ID CUSTOMER</td>
		<td>GRAND TOTAL</td>
		<td>TANGGAL</td>
	</tr>
	</thead>
	<tbody>
	<?php
	$no=0;
	foreach ($daftar_pesanan as $pesan): 
		$no++;?>
	<tr>
		<td><?=$no?></td>
		<td><?=$pesan->id_nota?></td>
		<td><?=$pesan->id_customer?></td>
		<td><?= number_format($pesan->grand_total)?></td>
		<td><?=$pesan->tgl_beli?></td>
		<!-- modal menampilkan detail barang yang dipesan-->
		<div class="modal fade" id="detail<?=$pesan->id_nota?>">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title">Modal title</h4>
		      </div>
		      
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Save changes</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		</td>
	</tr>
	<?php endforeach ?>
	</tbody>
</table></div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
 
</script>