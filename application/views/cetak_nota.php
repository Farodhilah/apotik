<h2 align="center">Nota transaksi</h2>
No Nota : <?= $nota->id_nota?><br>
Tanggal Beli : <?= $nota->tgl_beli?><br>

<table border="1" style="border-collapse: collapse;">
	<tr>
		<th>NO</th><th>Nama Obat</th><th>Harga</th><th>QTY</th><th>Subtotal</th>
	</tr>
	<?php $no=0; foreach ($this->trans->detail_transaksi($nota->id_nota) as $obat): $no++;?>
		<tr>
		<th><?=$no?></th>
		<th><?=$obat->nama_obat?></th>
		<th><?= number_format($obat->harga)?></th>
		<th><?=$obat->jumlah?></th>
		<th><?= number_format(($obat->harga*$obat->jumlah))?></th>
	</tr>
	<?php endforeach ?>
	<tr>
		<th colspan="4">Grand Total</th><th><?= number_format($nota->grand_total)?></th>
	</tr>
</table>

<script type="text/javascript">
	window.print();
	location.href="<?=base_url('index.php/transaksi')?>";
</script>
